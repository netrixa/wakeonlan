﻿using System;
using System.Net;
using System.Net.Sockets;

namespace WakeOnLan
{
    internal class Program
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                Console.WriteLine("Please provide a MAC address.");

                return;
            }

            try
            {
                var macAddress = GetMacAddressBytes(args[0]);

                var client = new UdpClient();

                client.Connect(IPAddress.Broadcast, 40000);

                var packet = new byte[17 * 6];

                for (var i = 0; i < 6; i++)
                    packet[i] = 0xFF;

                for (var i = 1; i <= 16; i++)
                {
                    for (var j = 0; j < 6; j++)
                        packet[i * 6 + j] = macAddress[j];
                }

                client.Send(packet, packet.Length);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }

            Console.WriteLine("The magic package was sent successfully.");
        }

        private static byte[] GetMacAddressBytes(string macAddressString)
        {
            const int macAddressLength = 6;
            const int macAddressFragmentSize = 2;
            var macAddress = new byte[macAddressLength];

            try
            {
                var macAddressFragments = macAddressString.Split(':', '-');

                if (macAddressFragments.Length != 6)
                {
                    macAddressFragments = macAddressString.Split('.');

                    if (macAddressFragments.Length == 3)
                    {
                        for (var i = 0; i < 3; i++)
                        {
                            macAddress[i * 2] = byte.Parse(macAddressFragments[i].Substring(0, macAddressFragmentSize), System.Globalization.NumberStyles.HexNumber);
                            macAddress[i * 2 + 1] = byte.Parse(macAddressFragments[i].Substring(2, macAddressFragmentSize), System.Globalization.NumberStyles.HexNumber);
                        }
                    }
                    else
                    {
                        for (var i = 0; i < 12; i += 2)
                            macAddress[i / 2] = byte.Parse(macAddressString.Substring(i, macAddressFragmentSize), System.Globalization.NumberStyles.HexNumber);
                    }
                }
                else
                {
                    for (var i = 0; i < 6; i++)
                        macAddress[i] = byte.Parse(macAddressFragments[i], System.Globalization.NumberStyles.HexNumber);
                }
            }
            catch
            {
                throw new ArgumentException("The MAC address doesn't have the correct format: " + macAddressString, "macAddressString");
            }

            return macAddress;
        }
    }
}
